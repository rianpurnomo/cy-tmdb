import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps/index";
// =======================================Scenario========================================================== //
//Change Language to Indonesia
Given('User open website TMDb', () => {
    cy.visit('https://www.themoviedb.org')
})
When('Click the language button and make sure the default language is English (US)', () => {
    // Click language tooltip
    cy.get("header div.flex div").click();
    // Click English Id for default language 
    cy.get("#sf2a9d2b-a2de-48e1-a512-cfc5f295869c").click();
})
And('select Indonesian', ()=> {
    // Input Indonesian search
    cy.get("div > div.k-animation-container input").type("indonesia");
    // Click Indonesia ID
    cy.get("#rb17d507-2944-4292-bf0c-da4d92dab066").click();
})
Then('click the Reload page button', ()=> {
    // Click reload page
    cy.get("div.k-tooltip-content a").click();
    // Page validation
    cy.location("href").should("eq", "https://www.themoviedb.org/");
})

// =======================================Scenario========================================================== //
// mark as favorite when they are login
When('Click search for the example film "part two"', ()=> {
    // Click button search
    cy.get("#inner_search_v4").click();
    // Input keyword title movie to search
    cy.get("#inner_search_v4").type("part two");
    // Click button search
    cy.get("#inner_search_form > input").click();
})
And('Select Movie to see the details', ()=> {
    // search validation
    cy.location("href").should("eq", "https://www.themoviedb.org/search?query=part+two");
    // Click detail movie
    cy.get("#card_movie_5e959bc3db72c00014ad69d6 h2").click();
    // detail movie validation
    cy.location("href").should("eq", "https://www.themoviedb.org/movie/693134-dune-part-two");
})
Then('click add to favorites', ()=> {
    // Click add to favorites
    cy.get("#favourite").click();
    // Copy wording validation
})

// =======================================Scenario========================================================== //
// User can mark as favorite a movie
When('User login dengan username dan password', ()=> {
    // Click button login
    cy.get("div.flex li:nth-of-type(3) > a").click();
    // Page validation
    cy.location("href").should("eq", "https://www.themoviedb.org/login");
    
    //inisialisasi env
    const username = Cypress.env('username');
    const password = Cypress.env('password');
    //Login
    cy.get('#username').type(username);
    cy.get('#password').type(password);
    cy.get('#login_form').submit();
    cy.get('img[src="/assets/2/v4/logos/v2/blue_short-8e7b30f73a4020692ccca9c88bafe5dcb6f8a62a4c6bc55cd9ba82bb2cd95f6c.svg"]').click();
    // Login validation
    cy.get('a[href="/u/rian-test"]')
    .should('exist')
    .should('have.attr', 'href', '/u/rian-test');
})
When('Click search for the example film "Part Two"', ()=> {
    // Click button search
    cy.get("#inner_search_v4").click();
    // Input keyword title movie to search
    cy.get("#inner_search_v4").type("part two");
    // Click button search
    cy.get("#inner_search_form > input").click();
    // search validation
    cy.location("href").should("eq", "https://www.themoviedb.org/search?query=part+two");
    cy.get('#search_form').scrollIntoView();
})
When('Select Movie to see movie details', ()=> {
    // Click detail movie
    cy.get("#card_movie_5e959bc3db72c00014ad69d6 h2").click();
    // validasi detail movie
    cy.location("href").should("eq", "https://www.themoviedb.org/movie/693134-dune-part-two");
})
When('Click add to favorite movies', ()=> {
    // Click add to favorites
    cy.get("#favourite").click();
    cy.get('#shortcut_bar_scroller').scrollIntoView();
})
And('Going the favorites menu', ()=> {
    // Going the favorites menu for validation
    cy.get("li.user span").click();
    cy.wait(5000)
    cy.visit(Url +'/u/rian-test/favorites')
    // Page validation
    cy.location("href").should("eq", "https://www.themoviedb.org/u/rian-test/favorites");
})
Then('Validate the films that are already', ()=> {
    // Validation movie
    cy.get('a[href="/movie/693134"]').should('exist');
    cy.contains('Dune: Part Two').should('exist')
    // remove movie from favorite
    cy.get("#card_movie_5e959bc3db72c00014ad69d6 li:nth-of-type(4) > a").click();
})

// =======================================Scenario========================================================== //
// User can favorite more than 1 movie
When('Select Movies to see movie details in trending categories', ()=> {      
    // Select movie 1
    cy.get("#trending_scroller > div > div:nth-of-type(1) img").click();
    cy.location("href").should("eq", "https://www.themoviedb.org/movie/636706-spaceman");
    
    // Click add to favorites
    cy.get("#favourite").click();
    cy.get('#shortcut_bar_scroller').scrollIntoView();
    cy.wait(2000)
    
    // Going to Home page
    cy.get("header img").click();
    
    // Home page validation
    cy.location("href").should("eq", "https://www.themoviedb.org/");
    
    // Select movie 2
    cy.get("section.trending div:nth-of-type(4) img").click();
    cy.location("href").should("eq", "https://www.themoviedb.org/movie/438631-dune");
    
    // Click add to favorites
    cy.get("#favourite").click();
    cy.get('#shortcut_bar_scroller').scrollIntoView();
    cy.wait(2000)
})
Then('Validate the films that are already on the list', ()=> {
    // Going the favorites menu for validation
    cy.get("li.user span").click();
    cy.wait(5000)
    cy.visit(Url +'/u/rian-test/favorites') 
    // Page validation
    cy.location("href").should("eq", "https://www.themoviedb.org/u/rian-test/favorites");
    // Validation movie 1
    cy.get('a[href="/movie/438631"]').should('exist');
    cy.contains('Dune').should('exist')
    // Validation movie 2
    cy.get('a[href="/movie/636706"]').should('exist');
    cy.contains('Spaceman').should('exist')
    // remove movie from favorite
    cy.get("#card_movie_58920d9b9251412dcb009489 li:nth-of-type(4) span > span").click();
    cy.wait(2000)
    cy.get("#card_movie_5d97c8c355937b00213358e0 li:nth-of-type(4) span > span").click();
})

// =======================================Scenario========================================================== //
// Users can remove movies from the favorite movies list in the favorite list
Then('Click button "Remove" to remove the movie from the Watchlist',()=>{
    // Validation movie
    cy.get('a[href="/movie/636706"]').should('exist');
    cy.contains('Spaceman').should('exist')
    // remove movie from favorite
    cy.get("#card_movie_5d97c8c355937b00213358e0 li:nth-of-type(4) span > span").click();
    cy.wait(2000);
})

// =======================================Scenario========================================================== //
// User can sort/order theri favorite movies lists
Then('Click the Order button to sort ascending or descending', ()=> {      

})

// =======================================Scenario========================================================== //
// Change language from Engilsh to Indonesia
When('Click the language button and make sure the default language is Indonesia', ()=> {
    // Click the Indonesian ID for the default language
    cy.get("#w782e9a0-d60a-41a6-b60e-675e0dab0367").click();
})
And('select English (US)', ()=> {
    // Input english search
    cy.get("div > div.k-animation-container input").type("english");
    // Click English ID
    cy.get("#lb5aeb6b-cf97-4844-977c-707613e92d2d").click(); 
})

// =======================================Scenario Additional========================================================== //
// View all page
When('View Movie populer', () => {      
    // Going to Movie Populer
    cy.get("div.nav_wrapper > ul > li.k-first > a").click();
    cy.get("li.k-first > div li.k-first > a").click();
    // page validation
    cy.location("href").should("eq", "https://www.themoviedb.org/movie");
    cy.wait(2000);
    // back to home or dashboard
    cy.get("header img").click();
    // page validation
    cy.location("href").should("eq", "https://www.themoviedb.org/");
    cy.wait(2000);
})
When('Change categories trending today to this week', () => {
    // Change trending categories
    cy.get("section.trending div.column_header div:nth-of-type(2) a").click();
    cy.wait(2000);
    cy.get("section.trending div.column_header > div > div > div:nth-of-type(1) a").click();
    cy.wait(2000);
})
When("Change Latest Trailers categories to Popular, Streaming, On TV, For Rent and In Theaters", () => {      
    // Change latest trailer categories
    cy.get("section.video div.column_header div:nth-of-type(2) a").click();
    cy.wait(2000);
    cy.get("section.video div.column_header div:nth-of-type(3) a").click();
    cy.wait(2000);
    cy.get("section.video div.column_header div:nth-of-type(4) a").click();
    cy.wait(2000);
    cy.get("div.column_header div:nth-of-type(5) a").click();
    cy.wait(2000);
    cy.get("section.video div.column_header > div > div > div:nth-of-type(1) a").click();
    cy.wait(2000);
})
Then('Change Free To Watch categories to TV and Movies', () => {      
    // Change Free to watch categories
    cy.get("section:nth-of-type(5) div.column_header div:nth-of-type(2) a").click();
    cy.wait(2000);
    cy.get("section:nth-of-type(5) div.column_header > div > div > div:nth-of-type(1) a").click();
    cy.wait(2000);  
})