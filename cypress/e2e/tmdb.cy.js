describe('Test Case TMDb', () => {
  // set variabel link yang akan diuji
  const Url= "https://www.themoviedb.org"
  // akan dijalankan setiap it()
  beforeEach(() => {
    // view port untuk tampilan
    // tampilan macbook
    // cy.viewport('macbook-16');
    // tampilan tablet
    cy.viewport(768, 1024);
    cy.visit(Url);
    // Cek log environtment untuk validasi username dan password terbaca
    cy.log(Cypress.env('username'))
    cy.log(Cypress.env('password'))
    cy.wait(2000);
  });
  // Title to describe
  describe( 'Scenario Test', () => {
    // Scenario 1
    it('Change language to Indonesia', () => {
      // Click language tooltip
      cy.get("header div.flex div").click();

      // Click English Id for default language
      // cy.get("#sf2a9d2b-a2de-48e1-a512-cfc5f295869c").click();
      
      // Enter Indonesian search
      // cy.get("div > div.k-animation-container input").type("indonesia");
      
      // Click Indonesia ID
      // cy.get("#rb17d507-2944-4292-bf0c-da4d92dab066").click();
      
      // Click reload page
      // cy.get("div.k-tooltip-content a").click();
      
      // Page validation
      // cy.location("href").should("eq", "https://www.themoviedb.org/");
      cy.wait(2000);
    });

    // Scenario 2
    it('Users can only "mark as favorite" when they are logged in', () => {
    // Click button search
    cy.get("#inner_search_v4").click();

    // Input keyword title movie to search
    cy.get("#inner_search_v4").type("part two");

    // Click button search
    cy.get("#inner_search_form > input").click();

    // search validation
    cy.location("href").should("eq", "https://www.themoviedb.org/search?query=part+two");
    cy.get('#search_form').scrollIntoView();

    // Click detail movie
    cy.get("#card_movie_5e959bc3db72c00014ad69d6 h2").click();

    // validasi detail movie
    cy.location("href").should("eq", "https://www.themoviedb.org/movie/693134-dune-part-two");
    
    // Click add to favorites
    cy.get("#favourite").click();
    cy.get('#shortcut_bar_scroller').scrollIntoView();
    cy.wait(2000);

    // Copy wording validation
    });

    // Scenario 3
    it('Users “mark as favorite” a movie', () => {
      // Click button login
      cy.get("div.flex li:nth-of-type(3) > a").click();
      // Page validation
      cy.location("href").should("eq", "https://www.themoviedb.org/login");
      
      //inisialisasi env
      const username = Cypress.env('username');
      const password = Cypress.env('password');
      //Login
      cy.get('#username').type(username);
      cy.get('#password').type(password);
      cy.get('#login_form').submit();
      cy.get('img[src="/assets/2/v4/logos/v2/blue_short-8e7b30f73a4020692ccca9c88bafe5dcb6f8a62a4c6bc55cd9ba82bb2cd95f6c.svg"]').click();
      // Login validation
      cy.get('a[href="/u/rian-test"]')
      .should('exist')
      .should('have.attr', 'href', '/u/rian-test');

      // Click button search
      cy.get("#inner_search_v4").click();

      // Input keyword title movie to search
      cy.get("#inner_search_v4").type("part two");

      // Click button search
      cy.get("#inner_search_form > input").click();

      // search validation
      cy.location("href").should("eq", "https://www.themoviedb.org/search?query=part+two");
      cy.get('#search_form').scrollIntoView();

      // Click detail movie
      cy.get("#card_movie_5e959bc3db72c00014ad69d6 h2").click();

      // validasi detail movie
      cy.location("href").should("eq", "https://www.themoviedb.org/movie/693134-dune-part-two");
      
      // Click add to favorites
      cy.get("#favourite").click();
      cy.get('#shortcut_bar_scroller').scrollIntoView();
      
      // Going the favorites menu for validation
      cy.get("li.user span").click();
      cy.wait(5000)
      cy.visit(Url +'/u/rian-test/favorites')
      
      // Page validation
      cy.location("href").should("eq", "https://www.themoviedb.org/u/rian-test/favorites");
      
      // Validation movie
      cy.get('a[href="/movie/693134"]').should('exist');
      cy.contains('Dune: Part Two').should('exist')
      
      // remove movie from favorite
      cy.get("#card_movie_5e959bc3db72c00014ad69d6 li:nth-of-type(4) > a").click();
      cy.wait(2000);
    });
    
    // Scenario 4
    it('Users can favorite more than 1 movie and validate the results', () => {
      // Click button login
      cy.get("div.flex li:nth-of-type(3) > a").click();
      // Page validation
      cy.location("href").should("eq", "https://www.themoviedb.org/login");
      
      //inisialisasi env
      const username = Cypress.env('username');
      const password = Cypress.env('password');
      //Login
      cy.get('#username').type(username);
      cy.get('#password').type(password);
      cy.get('#login_form').submit();
      cy.get('img[src="/assets/2/v4/logos/v2/blue_short-8e7b30f73a4020692ccca9c88bafe5dcb6f8a62a4c6bc55cd9ba82bb2cd95f6c.svg"]').click();
      // Login validation
      cy.get('a[href="/u/rian-test"]')
      .should('exist')
      .should('have.attr', 'href', '/u/rian-test');

      // Select movie 1
      cy.get("#trending_scroller > div > div:nth-of-type(1) img").click();
      cy.location("href").should("eq", "https://www.themoviedb.org/movie/636706-spaceman");
      
      // Click add to favorites
      cy.get("#favourite").click();
      cy.get('#shortcut_bar_scroller').scrollIntoView();
      cy.wait(2000)
      
      // Going to Home page
      cy.get("header img").click();
      
      // Home page validation
      cy.location("href").should("eq", "https://www.themoviedb.org/");
      
      // Select movie 2
      cy.get("section.trending div:nth-of-type(4) img").click();
      cy.location("href").should("eq", "https://www.themoviedb.org/movie/438631-dune");
      
      // Click add to favorites
      cy.get("#favourite").click();
      cy.get('#shortcut_bar_scroller').scrollIntoView();
      cy.wait(2000)

      // Going the favorites menu for validation
      cy.get("li.user span").click();
      cy.wait(5000)
      cy.visit(Url +'/u/rian-test/favorites')
      
      // Page validation
      cy.location("href").should("eq", "https://www.themoviedb.org/u/rian-test/favorites");
      
      // Validation movie 2
      cy.get('a[href="/movie/438631"]').should('exist');
      cy.contains('Dune').should('exist')

      // Validation movie 1
      cy.get('a[href="/movie/636706"]').should('exist');
      cy.contains('Spaceman').should('exist')
      
      // remove movie from favorite
      cy.get("#card_movie_58920d9b9251412dcb009489 li:nth-of-type(4) span > span").click();
      cy.wait(2000);
      cy.get("#card_movie_5d97c8c355937b00213358e0 li:nth-of-type(4) span > span").click();
      cy.wait(2000);
    });

    // Scenario 5
    it('Users can remove movies from the favorite movies list in the favorite list',()=> {
      // Click button login
      cy.get("div.flex li:nth-of-type(3) > a").click();
      // Page validation
      cy.location("href").should("eq", "https://www.themoviedb.org/login");
      
      //inisialisasi env
      const username = Cypress.env('username');
      const password = Cypress.env('password');
      //Login
      cy.get('#username').type(username);
      cy.get('#password').type(password);
      cy.get('#login_form').submit();
      cy.get('img[src="/assets/2/v4/logos/v2/blue_short-8e7b30f73a4020692ccca9c88bafe5dcb6f8a62a4c6bc55cd9ba82bb2cd95f6c.svg"]').click();
      // Login validation
      cy.get('a[href="/u/rian-test"]')
      .should('exist')
      .should('have.attr', 'href', '/u/rian-test');

      // Select movie
      cy.get("#trending_scroller > div > div:nth-of-type(1) img").click();
      cy.location("href").should("eq", "https://www.themoviedb.org/movie/636706-spaceman");
      
      // Click add to favorites
      cy.get("#favourite").click();
      cy.get('#shortcut_bar_scroller').scrollIntoView();
      cy.wait(2000)

      // Going the favorites menu for validation
      cy.get("li.user span").click();
      cy.wait(5000)
      cy.visit(Url +'/u/rian-test/favorites')
      
      // Page validation
      cy.location("href").should("eq", "https://www.themoviedb.org/u/rian-test/favorites");
      
      // Validation movie
      cy.get('a[href="/movie/636706"]').should('exist');
      cy.contains('Spaceman').should('exist')
      
      // remove movie from favorite
      cy.get("#card_movie_5d97c8c355937b00213358e0 li:nth-of-type(4) span > span").click();
      cy.wait(2000);

    });

    // Scenario 6
    it('Users can sort/order their favorite movies list', () => {
      // Click button login
      cy.get("div.flex li:nth-of-type(3) > a").click();
      // Page validation
      cy.location("href").should("eq", "https://www.themoviedb.org/login");
      
      //inisialisasi env
      const username = Cypress.env('username');
      const password = Cypress.env('password');
      //Login
      cy.get('#username').type(username);
      cy.get('#password').type(password);
      cy.get('#login_form').submit();
      cy.get('img[src="/assets/2/v4/logos/v2/blue_short-8e7b30f73a4020692ccca9c88bafe5dcb6f8a62a4c6bc55cd9ba82bb2cd95f6c.svg"]').click();
      // Login validation
      cy.get('a[href="/u/rian-test"]')
      .should('exist')
      .should('have.attr', 'href', '/u/rian-test');

      // Select movie 1
      cy.get("#trending_scroller > div > div:nth-of-type(1) img").click();
      cy.location("href").should("eq", "https://www.themoviedb.org/movie/636706-spaceman");
      
      // Click add to favorites
      cy.get("#favourite").click();
      cy.get('#shortcut_bar_scroller').scrollIntoView();
      cy.wait(2000)
      
      // Going to Home page
      cy.get("header img").click();
      
      // Home page validation
      cy.location("href").should("eq", "https://www.themoviedb.org/");
      
      // Select movie 2
      cy.get("section.trending div:nth-of-type(4) img").click();
      cy.location("href").should("eq", "https://www.themoviedb.org/movie/438631-dune");
      
      // Click add to favorites
      cy.get("#favourite").click();
      cy.get('#shortcut_bar_scroller').scrollIntoView();
      cy.wait(2000)

      // Going the favorites menu for validation
      cy.get("li.user span").click();
      cy.wait(5000)
      cy.visit(Url +'/u/rian-test/favorites')
      
      // Page validation
      cy.location("href").should("eq", "https://www.themoviedb.org/u/rian-test/favorites");
      
      // Validation movie 2
      cy.get('a[href="/movie/438631"]').should('exist');
      cy.contains('Dune').should('exist')

      // Validation movie 1
      cy.get('a[href="/movie/636706"]').should('exist');
      cy.contains('Spaceman').should('exist')
      
      // Click button to desc
      cy.get("div.title_header a.selected > span").click();
      cy.wait(3000);

      // Click button to asc
      cy.get("div.title_header a.selected > span").click();
      cy.wait(3000);

      // remove movie from favorite
      cy.get("#card_movie_58920d9b9251412dcb009489 li:nth-of-type(4) span > span").click();
      cy.wait(2000);
      cy.get("#card_movie_5d97c8c355937b00213358e0 li:nth-of-type(4) span > span").click();
      cy.wait(2000);
    });

    // Scenario 7
    it('Change language from Engilsh to Indonesia', () => {
      // Click language tooltip
      cy.get("header div.flex div").click();
      
      // Click the Indonesian ID for the default language
      // cy.get("#w782e9a0-d60a-41a6-b60e-675e0dab0367").click();
      
      // Input english search
      // cy.get("div > div.k-animation-container input").type("english");
      
      // Click English ID
      // cy.get("#lb5aeb6b-cf97-4844-977c-707613e92d2d").click(); 
      
      // Click reload page
      // cy.get("div.k-tooltip-content a").click();
     
      // Page validation
      // cy.location("href").should("eq", "https://www.themoviedb.org/");
    });
    // Scenario 8
    it('View all page', () => {
      // Going to Movie Populer
      cy.get("div.nav_wrapper > ul > li.k-first > a").click();
      cy.get("li.k-first > div li.k-first > a").click();

      // page validation
      cy.location("href").should("eq", "https://www.themoviedb.org/movie");
      cy.wait(2000);

      // back to home or dashboard
      cy.get("header img").click();
      // page validation
      cy.location("href").should("eq", "https://www.themoviedb.org/");
      cy.wait(2000);

      // Change trending categories
      cy.get("section.trending div.column_header div:nth-of-type(2) a").click();
      cy.wait(2000);
      cy.get("section.trending div.column_header > div > div > div:nth-of-type(1) a").click();
      cy.wait(2000);

      // Change latest trailer categories
      cy.get("section.video div.column_header div:nth-of-type(2) a").click();
      cy.wait(2000);
      cy.get("section.video div.column_header div:nth-of-type(3) a").click();
      cy.wait(2000);
      cy.get("section.video div.column_header div:nth-of-type(4) a").click();
      cy.wait(2000);
      cy.get("div.column_header div:nth-of-type(5) a").click();
      cy.wait(2000);
      cy.get("section.video div.column_header > div > div > div:nth-of-type(1) a").click();
      cy.wait(2000);

      // Change Free to watch categories
      cy.get("section:nth-of-type(5) div.column_header div:nth-of-type(2) a").click();
      cy.wait(2000);
      cy.get("section:nth-of-type(5) div.column_header > div > div > div:nth-of-type(1) a").click();
      cy.wait(2000);
    });
  });
});