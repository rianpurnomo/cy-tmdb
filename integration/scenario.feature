Feature: Automation Feature TMDb (The Movie Database) with Cypress
    
    Scenario: Change language to Indonesia
        Given User open website TMDb
        When Click the language button and make sure the default language is English (US)
        And select Indonesian
        Then click the Reload page button

    Scenario: Users can only "mark as favorite" when they are logged in
        Given users open the TMDb website
        When Click search for the example film "part two"
        And Select Movie to see the details
        Then click add to favorites

    Scenario: Users “mark as favorite” a movie
        Given User open website TMDb
        When User login dengan username dan password
        When Click search for the example film "Dune: Part Two"
        When Select Movie to see movie details
        When Click add to favorite movies
        And Going the favorites menu
        Then Validate the films that are already
    
    Scenario: Users can favorite more than 1 movie and validate the results
        Given User open website TMDb
        When User login dengan username dan password
        When Select Movies to see movie details in trending categories
        When Click add to favorite movies
        And Going the favorites menu
        Then Validate the films that are already on the list
    
    Scenario: Users can remove movies from the favorite movies list in the favorite list
        Given User open website TMDb
        When User login dengan username dan password
        And Going the favorites menu
        Then Click button "Remove" to remove the movie from the Watchlist
    
    Scenario: Users can sort/order their favorite movies list
        Given User open website TMDb
        When User login dengan username dan password
        And Going the favorites menu
        Then Click the Order button to sort ascending or descending
    
    Scenario: Change language from Engilsh to Indonesia
        Given User open website TMDb
        When Click the language button and make sure the default language is Indonesia
        And select English (US)
        Then click the Reload page button

    Scenario: View all page
        Given User open website TMDb
        When View Movie populer
        When Change categories trending today to this week
        And Change Latest Trailers categories to Popular, Streaming, On TV, For Rent and In Theaters
        Then Change Free To Watch categories to TV and Movies
        