<!-- ====================================== Welcome ====================================== -->
# description
Registrasi & Testing TheMovieDb API - Dicoding Blog
The Movie Database (TMDb) adalah database film yang menyediakan data-data lengkap seperti data film yang akan datang, data serial tv (TvSeries), dll. Dan tentunya semua itu bisa kita gunakan secara gratis.

# Purpose
- ini digunakan untuk megerjakan test technical dan mengasah kemampuan saya dalam automation menggunakan cypress
- pembuatan scenario dengan gherkin
- menjalankan automation sesuai dengan scenario

# Scenario automation
1. Ubah bahasa menjadi bahasa indonesia
2. User hanya bisa melakukan ”mark as favorite” ketika dia sudah login
3. Ketika user melakukan “mark as favorite” dari suatu film, maka film tersebut akan tersimpan di bagian favorite movies di profil pengguna
4. User bisa memfavorite lebih dari 1 movie dan validasi hasilnya
5. User bisa meremove movie dari daftar favorite movies di favorite list
6. User bisa mengurutkan / ordering daftar favorite movies dia
7. Ubah kembali ke bahasa Inggris lalu lakukan regresi test kembali

# Skenario pengujian dalam format Gherkin
- buka pada folder integration, nama folder scenario.feature
- scenario sudah termasuk wajib dan additional
- untuk melihat step scenario yang digunakan bisa dilihat pada file cypress/support/steps.js
- untuk mengganti viewport / tampilan saat menjalankan pengujian bisa diubah pada file cypress/e2e/tmdb.cy.js dan ubah pada script cy.viewport(''), diisi sesuai resolusi device
- seluruh file script e2e bisa dilihat pada cypress/e2e/tmdb.cy.js

# Kendala
- Scenario ubah bahasa pada point 1 dan 7 belum bisa dijalankan karena pada id bahasa, id yang digunakan dinamis, sehingga tidak bisa digunakan sebagai element untuk automation
- Scenario point 2 kurang divalidasi pada bagian copy wording, karena copy wording informasi mengenai "harus login sebelum melakukan mark as favorite" tidak bisa diambil sebagai contains untuk validasi
- Selain 2 point diatas, sudah dikerjakan sebagaimana mestinya/

# Hal yang perlu diperhatikan
- ubah username dan password pada file cypress.env.json
apabila tidak tersedia file tersebut, dapat dibuat manual dengan nama cypress.env.json dengan isinya 
{
    "username": "username",
    "password": "password"
  }
  
- pastikan dilaptop Saudara sudah terinstall package yang dibutuhkan / bisa mengikuti langakah-langakah yang sudah dicantumkan
- pastikan tidak ada movies yang sudah di mark as favorite untuk menghindari error running (bisa menggunakan akun baru)
- pastikan koneksi dalam kondisi stabil

# prepare sebelum menjalankan, pastikan sudah menginstal node, npm dan cypress, versi terbaru boleehh
# berikut contoh install dengan terminal mac atau jika windows bisa menyesuaikan :
- install node 
- install npm (brew install npm)
- npm -v (check version npm)
- npm install
- npm install cypress --save-dev
- note : jika sudah pernah menginstall node dan npm, langsung saja jalankan file

# Cara menjalaankan program :
- ketik "npm i" => untuk menginstall package sesuai dengan yang di file ini
- ketik "npx cypress open" (dengan tampilan UI) pada terminal -> pilih e2e testing, pilih folder e2e, pilih file tmdb.cy.js, maka akan otomatis running
- ketik "npx cypress run" (tanpa tampilan UI) pada terminal


<!-- ====================================== Terima kasih ====================================== -->